export const appConfig = {
  apiUrl: 'http://localhost:3000/api',
  tasks_per_page: 6,
  paginator_links_num: 7, // number of links of paginator
};
