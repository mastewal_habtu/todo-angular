// interface of login response object based on loopback

interface LoginResponse {
  id: string;
  ttl: number;
  created: string;
  userId: string;
}
