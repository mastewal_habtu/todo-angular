import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {appConfig} from "../app.config";

import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

import {User} from "../models/user";

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {}

  login(user: User): Observable<LoginResponse> {
    return this.http
      .post(appConfig.apiUrl + '/users/login', user)
      .map((res) => {
        // login successful if there's a jwt access token in the response
        const response = res as LoginResponse;
        if (response && response.id) {  // response.id is awkwardly the access token
          // store user details and jwt access token in local storage to keep user logged in
          localStorage.setItem('login', JSON.stringify(response));
        }

        return response;
      });
  }

  logout(): void {
    // remove user from local storage to log user out
    localStorage.removeItem('login');

    // Actually clearing local storage will effectively log user out
    // since angular consider user logged in based on local storage
    // but whatever, let's logout user from the server anyway
    this.http.post(appConfig.apiUrl + '/users/logout', null).subscribe();
  }
}
