import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {appConfig} from "../app.config";

@Injectable()
export class FilesService {

  constructor(private http: HttpClient) {
  }

  upload(container_name: string, formData: FormData) {
    const upload_url = `/containers/${container_name}/upload`;

    let headers = new Headers();
    headers.delete('Content-Type');

    return this.http.post(appConfig.apiUrl + upload_url, formData);
  }
}
