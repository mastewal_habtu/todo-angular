import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {appConfig} from "../app.config";
import {Subject} from "rxjs/Subject";
import {ReplaySubject} from "rxjs/ReplaySubject";

import "rxjs/add/operator/take";
import {AlertService} from "./alert.service";
import {Task} from "../models/task";

@Injectable()
export class TasksService {
  userId: string = JSON.parse(localStorage.getItem('login')).userId;


  constructor(
    private http: HttpClient,
    private alertService: AlertService
  ) {}

  getTasks(skip_index: number = 0): Observable<Task[]> {
    return this.http.get(appConfig.apiUrl + `/users/${this.userId}/tasks?filter[skip]=${skip_index}`);
  }

  postTask(new_task: Task): Observable<Task> {
    return this.http.post(appConfig.apiUrl + '/tasks', new_task);
  }

  patchTask(task_id: string, task_part: any): Observable<Task> {
    return this.http.patch(appConfig.apiUrl + `/tasks/${task_id}`, task_part);
  }

  deleteTask(task_id: string): Observable<object> {
    return this.http.delete(appConfig.apiUrl + `/tasks/${task_id}`);
  }

  countTasks(): Observable<any> {
    return this.http.get(appConfig.apiUrl + `/users/${this.userId}/tasks/count`);
  }

}
