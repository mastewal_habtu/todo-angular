import { Injectable } from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";

import {User} from "../models/user";

import {appConfig} from "../app.config";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  postUser(user: User): Observable<User> {
    return this.http.post(appConfig.apiUrl + '/users', user);
  }

  putUser(user: User): Observable<User> {
    return this.http.put(appConfig.apiUrl + '/users/' + user.id, user);
  }

  patchUser(user: User): Observable<User> {
    return this.http.patch(appConfig.apiUrl + '/users/' + user.id, user);
  }

  deleteUser(user_id: string): Observable<object> {
    return this.http.delete(appConfig.apiUrl + '/users/' + user_id);
  }

  getUser(user_id: string): Observable<User> {
    return this.http.get(appConfig.apiUrl + '/users/' + user_id);
  }

  checkUsernameExists(username: string): Observable<boolean> {
    // let's put username in parameter
    let params: HttpParams = new HttpParams();
    params = params.set('username', username);

    return this.http.get(
      appConfig.apiUrl + '/users/username_exists', {params: params})
      .map((res: any) => res.exists);
  }
}


