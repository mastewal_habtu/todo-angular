import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AccessTokenAuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // get the current logged in user
    const currentUser = JSON.parse(localStorage.getItem("login"));

    // if user is logged in, include access token in header of request
    if (currentUser && currentUser.id) {
      const authReq = req.clone({
        setHeaders: {'Authorization': currentUser.id}
      });

      return next.handle(authReq);
    }

    return next.handle(req);
  }
}
