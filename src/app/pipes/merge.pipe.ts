import {Pipe, PipeTransform} from "@angular/core";
import {Task} from "../models/task";


// pipe to merge to arrays of any type
@Pipe({
  name: 'merge',
  pure: false
})
export class MergePipe implements PipeTransform {
  transform<T>(arr1: T[], arr2: T[]): T[] {
    return arr1.concat(arr2);
  }
}
