export function get_error_message(err: any, default_message: string = "Error occured.") {
  if (err) {
    // check if it is loopback http error response
    if (err.error && err.error.error && err.error.error.message) {
      return err.error.error.message;
    } else if (err.status === 0) {  // check if there was some kind of network error or...
      return "Unable to connect.";
    }
  }

  // default handling of error message
  return default_message;
}
