import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { AuthGuard } from "./guards/auth.guard";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { TaskListComponent } from "./components/task_list/task_list.component";
import { AlertComponent} from "./components/alert/alert.component";

import { AppRoutingModule } from "./app.routing";

import {AlertService, AuthenticationService, TasksService, UserService, FilesService} from "./services/index";
import {AccessTokenAuthInterceptor} from "./interceptors/accessTokenAuth.interceptor";
import {LoggedInComponent} from "./components/loggedIn/loggedIn.component";
import {LoggedInRoutingModule} from "./components/loggedIn/loggedIn.routing";

import {MessageService} from "primeng/components/common/messageservice";
import {GrowlModule, MessageModule, MessagesModule} from "primeng/primeng";

import {Ng2FileTypeModule} from "ng2-file-type";
import {ProfileComponent} from "./components/profile/profile.component";
import {MergePipe} from "./pipes/merge.pipe";
import { TaskComponent } from './components/task/task.component';
import { PaginationComponent } from './components/pagination/pagination.component';



@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    TaskListComponent,
    LoggedInComponent,
    ProfileComponent,
    MergePipe,
    TaskComponent,
    PaginationComponent,
  ],
  imports: [
    BrowserModule,
    // BrowserAnimationsModule, has some bug of not replacing nested router-outlet appropriately
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LoggedInRoutingModule,
    AppRoutingModule,
    // primeng modules
    MessagesModule,
    MessageModule,
    GrowlModule,
    // file type validator
    Ng2FileTypeModule
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    TasksService,
    UserService,
    FilesService,
    MessageService, // primeng service
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AccessTokenAuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
