import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AlertService, UserService} from "../../services/index";
import {User} from "../../models/user";
import {MessageService} from "primeng/components/common/messageservice";

@Component({
  selector: "register",
  templateUrl: "register.component.html"
})

export class RegisterComponent {
  new_user: User = new User();  // model to put new user details
  loading = false;

  constructor(private router: Router,
              private alertService: AlertService,
              private userService: UserService,
              private messageService: MessageService) {
  }

  register() {
    this.loading = true;
    this.userService.postUser(this.new_user)
      .subscribe(
        data => {
          this.messageService.add(
            {severity: 'success', summary: 'Registration successful'});
          this.router.navigate(['/login']);
        },
        error => {
          // check if a user already exists with the same username
          this.userService.checkUsernameExists(this.new_user.username)
            .subscribe(
              res => {
                if (res === true) {
                  this.messageService.add(
                    {severity: 'error', summary: "A user with specified username already exists"});
                } else {
                  this.messageService.add(
                    {severity: 'error', summary: error}
                  );
                }
              },
              err => this.messageService.add({severity: 'error', summary: error}
              )
            );
          this.loading = false;
        }
      );
  }
}
