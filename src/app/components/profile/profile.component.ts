import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user";
import {FilesService} from "../../services/files.service";
import {UserService} from "../../services/user.service";
import {MessageService} from "primeng/components/common/messageservice";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  'selector': 'profile',
  'templateUrl': 'profile.component.html'
})

export class ProfileComponent implements OnInit {
  myForm: FormGroup;
  email: FormControl;
  picture: FormControl;
  user: User;

  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(private fb: FormBuilder,
              private filesService: FilesService,
              private userService: UserService,
              private messageService: MessageService,
              private authService: AuthenticationService) {
    this.user = JSON.parse(localStorage.getItem('user')) as User;
    if (!this.user) {
      this.authService.logout();  // logout if there is not user detail
    }

    this.myForm = this.fb.group({
      'email': [this.user.email, Validators.required],
    });
  }

  ngOnInit() {

  }

  update() {
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      const formData = new FormData();
      formData.append("name", fileBrowser.files[0]);
      this.filesService.upload("profile_pictures", formData)
        .subscribe(
          (data: any) => {
            console.log("upload response: ", data);
            this.updateUserModel(data.result.files.name[0].name); // awkward response from server
          },
          err => {
            console.log("upload failed: ", err);
          }
        );
    } else {
      this.updateUserModel();
    }
  }

  updateUserModel(picture_name?: string) {
    // if a new picture name is found update user's picture
    if (picture_name) {
      this.user.picture = picture_name;
    }

    this.user.email = this.myForm.controls.email.value;

    this.userService.patchUser(this.user).subscribe(
      res => {
        // update user model in local storage
        localStorage.setItem('user', JSON.stringify(this.user));

        this.messageService.add(
          {severity: 'success', summary: 'Update successful'}
        );
      }, err => {
        console.log("update failed: ", err);
        this.messageService.add(
          {severity: 'error', summary: 'Update failed'}
        );
      }
    );
  }


}
