import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {TasksService} from "../../services/tasks.service";
import {MessageService} from "primeng/components/common/messageservice";
import {get_error_message} from "../../helper/error_handler";
import {ActivatedRoute} from "@angular/router";
import {appConfig} from "../../app.config";
import {current} from "codelyzer/util/syntaxKind";

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  current_page = 1;
  total_pages = 1;
  count = 0;

  // simple convetional array to hold page information
  // -1 means show ... as a page to fill a space of many pages
  // otherwise the page number is stored in order
  pages: number[] = [];

  page_items_num = appConfig.tasks_per_page;  // number of items per page
  paginator_links_num = appConfig.paginator_links_num;  // max number of page links in a paginator

  @Input() reload;  // tell if the pagination need to reload

  constructor(private taskService: TasksService,
              private messageService: MessageService,
              private route: ActivatedRoute) {
    this.route.params.subscribe(
      params => {
        if (params['page']) {
          this.current_page = +params['page'];
        } else {
          this.current_page = 1;
        }

        this.setPages();
      },
      err => {
        console.log("route params subscription failed.");
      }
    );
  }

  // update pagination when there is a state change
  ngOnChanges() {
    this.updatePagination();
  }

  ngOnInit() {
    this.updatePagination();
  }

  updatePagination() {
    this.taskService.countTasks()
      .subscribe(
        res => {
          if (this.get_total_pages(this.count) < this.get_total_pages(res.count)) {
            this.count = res.count;
            this.setPages();
          }
        },
        err => this.messageService.add({
          severity: 'error', summary: 'Failed to count tasks.',
          detail: get_error_message(err)
        })
      );
  }

  get_total_pages(count: number) {
    return Math.ceil(count / this.page_items_num);
  }

  setPages() {
    this.pages = [];

    this.total_pages = this.get_total_pages(this.count);

    if (this.total_pages <= this.paginator_links_num) {
      for (let page = 1; page <= this.total_pages; page++) {
        this.pages.push(page);
      }
    } else if (this.checkInfrontPages()) {
      for (let page = 1; page <= (this.paginator_links_num - 2); page++) {
        this.pages.push(page);
      }

      this.pages.push(-1); // fill a ... rather than page number
      this.pages.push(this.total_pages);  // the last page
    } else if (this.checkLastPages()) {
      this.pages.push(1);  // the first page
      this.pages.push(-1); // fill a ... rather than page number

      for (let page = this.total_pages - (this.paginator_links_num - 2); page < this.total_pages; page++) {
        this.pages.push(page);
      }
    } else {
      this.pages.push(1); // the first page
      this.pages.push(-1); // fill a ... rather than page number

      let remaining_pages = this.paginator_links_num - 4;
      for (let page = this.current_page - Math.floor(remaining_pages / 2);
           page < this.current_page + Math.ceil(remaining_pages / 2); page++) {
        this.pages.push(page);
      }

      this.pages.push(-1); // fill a ... rather than page number
      this.pages.push(this.total_pages); // the last page
    }
  }

  // check if current page is one of the pages at start excluding the two last paginator links
  checkInfrontPages() {
    return this.current_page < (this.paginator_links_num - 2);
  }

  // check if current page is one of the pages at end excluding the two first paginator links
  checkLastPages() {
    return this.current_page > this.total_pages - (this.paginator_links_num - 2);
  }
}
