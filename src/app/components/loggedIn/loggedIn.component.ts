import {Component} from "@angular/core";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {User} from "../../models/user";
import {appConfig} from "../../app.config";

@Component({
  "selector": "logged-in",
  "templateUrl": "loggedIn.component.html"
})

export class LoggedInComponent {
  user: User;
  apiUrl: string;

  constructor(private authService: AuthenticationService,
              private router: Router,) {
    this.user = JSON.parse(localStorage.getItem('user')) as User;
    if (!this.user) {
      this.authService.logout();  // logout if there is not user detail
    }

    console.log('user: ', this.user);

    this.apiUrl = appConfig.apiUrl;
  }

  logout(): void {
    this.authService.logout();

    this.router.navigate(['/login']);
  }
}
