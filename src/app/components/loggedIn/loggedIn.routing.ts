import {RouterModule, Routes} from "@angular/router";
import {LoggedInComponent} from "./loggedIn.component";
import {TaskListComponent} from "../task_list/task_list.component";
import {AuthGuard} from "../../guards/auth.guard";
import {NgModule} from "@angular/core";
import {ProfileComponent} from "../profile/profile.component";

const loggedInRoutes: Routes = [
  {
    path: '',
    component: LoggedInComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'tasks',
        component: TaskListComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: '', redirectTo: 'tasks', pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(loggedInRoutes) ],
  exports: [ RouterModule ]
})

export class LoggedInRoutingModule {}
