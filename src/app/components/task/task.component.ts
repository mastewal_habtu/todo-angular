import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Task} from "../../models/task";
import {TasksService} from "../../services/tasks.service";
import {MessageService} from "primeng/components/common/messageservice";
import {get_error_message} from "../../helper/error_handler";

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input('task') task: Task;

  @Output() taskDeleted = new EventEmitter<Task>();

  isEditing = false;
  formerDescription: string;  // variable to recover edited task description in case of error


  constructor(private taskService: TasksService,
              private messageService: MessageService) {
  }

  ngOnInit() {
  }

  deleteTask(): void {
    this.taskService.deleteTask(this.task.id)
      .subscribe(
        res => {
          this.taskDeleted.emit(this.task);
        },
        err => {
          this.messageService.add({
            severity: 'error', summary: 'Failed to delete task.',
            detail: get_error_message(err)
          });
        }
      );
  }

  // change the state of task.isDone
  changeTaskState(): void {
    this.taskService.patchTask(this.task.id, {isDone: this.task.isDone})
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          this.messageService.add({
            severity: 'error', summary: "Failed to change state.",
            detail: get_error_message(err)
          });
          // reverse the damage done, it was just for nothing
          this.task.isDone = !this.task.isDone;
        }
      );
  }

// change task.description of a task
  editTaskDescription(): void {
    this.taskService.patchTask(this.task.id, {description: this.task.description})
      .subscribe(
        res => {
          console.log(res);
          this.isEditing = false;
        },
        err => {
          // reverse the change made on task.description to its former state
          this.task.description = this.formerDescription;
          this.messageService.add({
            severity: 'error', summary: "Failed to edit task.",
            detail: get_error_message(err)
          });
        }
      );
  }
}
