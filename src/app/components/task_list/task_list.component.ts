import {Component, OnInit} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";

import {TasksService, AlertService} from "../../services/index";
import {Task} from "../../models/task";
import {MessageService} from "primeng/components/common/messageservice";
import {get_error_message} from "../../helper/error_handler";
import {ActivatedRoute} from "@angular/router";
import {appConfig} from "../../app.config";


@Component({
  templateUrl: 'task_list.component.html',
  styleUrls: ['task_list.component.css'],
  selector: 'task-list'
})

export class TaskListComponent implements OnInit {
  tasks: Task[] = []; // task_list got from api
  new_task: Task = new Task();

  reload_pagination = true;  // toggle to notify change and reloading of child

  constructor(private taskService: TasksService,
              private messageService: MessageService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      params => {
        if (params['page']) {
          this.getTasks(params['page']);
        } else {
          this.getTasks();
        }
      }
    );
  }

  getTasks(page: number = 1): void {
    let skip_index = (page - 1) * appConfig.tasks_per_page;
    this.taskService.getTasks(skip_index)
      .subscribe(
        tasks => this.tasks = tasks,
        (err) => this.messageService.add({
          severity: 'error', summary: "Failed to list tasks.",
          detail: get_error_message(err)
        })
      );
  }

  addTask(): void {
    this.taskService.postTask(this.new_task)
      .subscribe(
        task => {
          this.tasks.unshift(task);
          this.reload_pagination = !this.reload_pagination; // toggle to notify child of a change
        },
        (err) => {
          this.messageService.add({
            severity: 'error', summary: "Failed to add task.",
            detail: get_error_message(err)
          });
        }
      );
  }

  removeTask(task: Task): void {
    // search task in this.task_list and delete if found
    let index = this.tasks.indexOf(task);
    if (index !== -1) {
      this.tasks.splice(index, 1);
      return;
    } else {
      console.log("task couldnot be found");
    }
  }

}
