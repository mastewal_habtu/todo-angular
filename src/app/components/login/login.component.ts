import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService, AlertService} from "../../services/index";
import {User} from "../../models/user";
import {HttpErrorResponse} from "@angular/common/http";
import {UserService} from "../../services/user.service";
import {MessageService} from "primeng/components/common/messageservice";
import {Message} from "primeng/primeng";

@Component({
  "selector": "login",
  "templateUrl": "login.component.html"
})

export class LoginComponent implements OnInit {
  user: User = new User();  // model to get user credential
  loading = false;
  returnUrl: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authService: AuthenticationService,
              private userService: UserService,
              private alertService: AlertService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    // reset login status
    this.authService.logout();

    // get return url from route parameters or default to '/task_list'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.authService.login(this.user)
      .subscribe(
        data => {
          // get user details if successful and store it locally
          this.userService.getUser(data.userId).subscribe(
            user => localStorage.setItem('user', JSON.stringify(user)),
            err => console.log("Can't get user details: ", err)
          );

          this.router.navigate([this.returnUrl]);
        },
        (error_response: HttpErrorResponse) => {
          // check if user exists
          this.userService.checkUsernameExists(this.user.username)
            .subscribe(
              res => {
                if (res === true) {
                  this.messageService.add(
                    {severity: 'error', summary: 'Password is incorrect'}
                  );
                } else {
                  this.messageService.add(
                    {severity: 'error', summary: "User doesnot exist."}
                  );
                }
              },
              err => {
                console.log("message service");
                this.messageService.add(
                  {severity: 'error', summary: 'Login failed', detail: ''});
              });
          this.loading = false;
        }
      );
  }
}
