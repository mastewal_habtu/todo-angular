import {Component, OnInit} from "@angular/core";
import {AlertService} from "../../services/index";

// let's use jquery, oh yeah!!
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'alert',
  templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit {
  message: any;

  constructor(private alertService: AlertService) {
  }

  ngOnInit() {
    this.alertService.getMessage().subscribe(
      message => {
        this.message = message;
      });
  }
}
