// model Task

export class Task {
  id: string;
  description: string;
  userId: string;
  isDone: boolean;
  createdAt: string;
  updatedAt: string;
}
